package com.example.contentproviderexample;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Adapter extends RecyclerView.Adapter<ViewHolder> {

    private List<ContactEntity> contactEntities;
    private LayoutInflater inflater;

    public Adapter(List<ContactEntity> contactEntities, LayoutInflater inflater) {
        this.contactEntities = contactEntities;
        this.inflater = inflater;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(inflater.inflate(R.layout.item_contact, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(contactEntities.get(i));
    }

    @Override
    public int getItemCount() {
        return contactEntities.size();
    }
}
