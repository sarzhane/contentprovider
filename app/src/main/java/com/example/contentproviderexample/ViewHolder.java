package com.example.contentproviderexample;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolder extends RecyclerView.ViewHolder {
    TextView id;
    TextView name;
    TextView phoneNumber;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        id = itemView.findViewById(R.id.id);
        name = itemView.findViewById(R.id.name);
        phoneNumber = itemView.findViewById(R.id.phoneNumber);
    }

    public void bind(ContactEntity contactEntity){
        this.id.setText(contactEntity.getId());
        this.name.setText(contactEntity.getName());
        this.phoneNumber.setText(contactEntity.getPhoneNumber());
    }
}
