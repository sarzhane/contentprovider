package com.example.contentproviderexample;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "sms_and_contacts";
    ArrayList<ContactEntity> myContacts = new ArrayList<>();
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button getContacts = findViewById(R.id.textButton);
        getContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printContactList();
            }
        });

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new Adapter(myContacts, LayoutInflater.from(this)));
    }
    private void printContactList() {
        int hasPermission = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS);
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            Log.w(TAG, "READ_CONTACTS permission is not granted. You should grant it for this app");
            return;
        }
        ContentResolver cr = getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if ((cursor != null ? cursor.getCount() : 0) > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(
                        cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cursor.getInt(cursor.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor phoneCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (phoneCur.moveToNext()) {
                        String phoneNo = phoneCur.getString(phoneCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        Log.i(TAG, "Name: " + name);
                        Log.i(TAG, "Phone Number: " + phoneNo);

                        ContactEntity contactEntity = new ContactEntity();
                        contactEntity.setId(id);
                        contactEntity.setName(name);
                        contactEntity.setPhoneNumber(phoneNo);
                        myContacts.add(contactEntity);
                        Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
                    }
                    phoneCur.close();
                }

            }
        }
        if (cursor != null) {
            cursor.close();
        }
    }
}
